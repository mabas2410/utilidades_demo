import XCTest
@testable import Utilidades

final class UtilidadesTests: XCTestCase {
    func testExample() {

		XCTAssertFalse(Utilidades().checkEmail(email: "s@s.s"))
		XCTAssertTrue(Utilidades().checkEmail(email: "mv@test.com"))
    }
	

    static var allTests = [
        ("testExample", testExample),
    ]
}
