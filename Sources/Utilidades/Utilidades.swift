import Foundation

public struct Utilidades {
    var text = "Hello, World!"
	 public init() {}
	public func localizedString(_ string: String) -> String {
		string.localized()
	}
	
	public func checkEmail(email: String) -> Bool {
		return email.isEmail
	}
	
	
}

extension String {
	func localized() -> String {
		return self
	}
	
	var isEmail: Bool {
		let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
		return emailPredicate.evaluate(with: self)
	}
}

public class Movil {
	public init(motor: String) {
		self.motor = motor
	}
	var motor: String
}

open class Animal {
	var especie: String
	public init(especie: String) {
		self.especie = especie
	}
}

public protocol Authorizable {
	var email: String { get set }
	var password: String { get set }
}


